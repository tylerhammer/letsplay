const authenticationController = require('./controllers/authenticationController')
const authenticationControllerPolicy = require('./policies/authenticationControllerPolicy')
const gamesController = require('./controllers/gamesController')

module.exports = (app) => {
    app.post('/register',
      authenticationControllerPolicy.register,
      authenticationController.register)
    app.post('/login',
        authenticationController.login)

    app.get('/games',
    gamesController.index)
    app.post('/games',
    gamesController.post)
}
