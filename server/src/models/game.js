module.exports = (sequelize, DataTypes) => {
    const Game = sequelize.define('Game', {
        sTitle: DataTypes.STRING,
        iMinPlayers: DataTypes.INTEGER,
        iMaxPlayers: DataTypes.INTEGER,
        sDifficulty: DataTypes.STRING,
        iSuggestedTime: DataTypes.INTEGER,
        sOwner: DataTypes.STRING,
        sDescription: DataTypes.TEXT,
        sGenre: DataTypes.STRING,
        sCategory: DataTypes.STRING,
        iBGG: DataTypes.INTEGER
    })

    return Game
}
