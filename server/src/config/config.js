const Sequelize = require('sequelize')
const Op = Sequelize.Op

const path = require('path')

module.exports = {
    port: process.env.PORT || 8081,
    db: {
        database: process.env.DB_NAME || 'letsplay',
        user: process.env.DB_USER || 'letsplay',
        password: process.env.DB_PASS || 'letsplay',
        options: {
            dialect: process.env.DIALECT || 'sqlite',
            host: process.env.HOST || 'localhost',
            storage: './letsplay.sqlite',
            operatorsAliases: Op
        }
    },
    authentication: {
        jwtSecret: process.env.JWT_SECRET || 'secret'
    }
}
