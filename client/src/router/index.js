import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import register from '@/components/register'
import login from '@/components/login'
import games from '@/components/games'
import addGames from '@/components/addGames'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'root',
            component: home
        },
        {
            path: '/register',
            name: 'register',
            component: register
        },
        {
            path: '/login',
            name: 'login',
            component: login
        },
        {
            path: '/games',
            name: 'games',
            component: games
        },
        {
            path: '/games/add',
            name: 'games-add',
            component: addGames
        }

    ]
})
