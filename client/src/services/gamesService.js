import api from '@/services/api'

export default {
    index () {
        return api().get('games')
    },
    post (game) {
        return api().post('games', game)
    }
}
